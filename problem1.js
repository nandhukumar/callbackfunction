let fs = require("fs");

let path = require("path");

function createDirectory(directoryName) {
  fs.mkdirSync(directoryName, function (err) {
    if (err) {
      console.log(err);
    }
    console.log("Wow! You've created the directory. Enjoyy!!");
  });
}

function createFiles(directoryName, countOfFiles) {
  let count = 1;
  let intervalId = setInterval(function () {
    let fileName = "file" + count + ".json";
    let filePath = path.join(__dirname, directoryName, fileName);
    if (count === countOfFiles + 1) {
      clearInterval(intervalId);
    } else {
      fs.writeFile(
        filePath,
        `${fileName} created successfully`,
        function (err) {
          if (err) {
            console.log(err);
          }
          console.log(`${fileName} created successfully`);
        }
      );
    }
    count += 1;
  }, 1);
}

function deleteFiles(directoryName) {
  let filePath = path.join(__dirname, directoryName);
  let arrayOfFiles = fs.readdirSync(filePath);

  let deletingEachFiles = arrayOfFiles.map((eachFile) => {
    let filePath = path.join(__dirname, directoryName, eachFile);
    fs.unlink(filePath, function (err) {
      if (err) {
        console.log(err);
      }
      console.log(`${eachFile} deleted successfully`);
    });
  });
}

// console.log(path.join(__dirname, "cg"));

function callbackFunctionToCreateAndDeleteFiles(directoryName, countOfFiles) {
  createDirectory(directoryName);
  createFiles(directoryName, countOfFiles);
  setTimeout(function () {
    deleteFiles(directoryName);
  }, 2000);
}

module.exports = callbackFunctionToCreateAndDeleteFiles;
