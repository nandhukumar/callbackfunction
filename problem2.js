let fs = require("fs");
let path = require("path");

function callbackFunctionProblem2(file) {
  let FileNames = "fileNames.txt";

  function deleteAllFilesByFileName() {
    let fileNamesPath = path.join(__dirname, FileNames);

    fs.readFile(fileNamesPath, "utf-8", function (err, data) {
      if (err) {
        console.log(err);
      } else {
        data = data.trim();
        let arrayOfFileNames = data.split("\n");
        let deleteEachFile = arrayOfFileNames.map((eachFileName) => {
          let deleteFilePath = path.join(__dirname, eachFileName);
          fs.unlink(deleteFilePath, function (err) {
            if (err) {
              console.log(err);
            } else {
              console.log(`${eachFileName} Deleted Successfully`);
            }
          });
        });
      }
    });
  }

  function writeFileNameToFileNames(fileName) {
    // let FileNames = "fileNames.txt";
    let fileNamePath = path.join(__dirname, FileNames);

    fs.appendFile(fileNamePath, fileName + "\n", function (err) {
      if (err) {
        console.log(err);
      } else {
        console.log(`Added ${fileName} to fileNames.txt`);
      }
    });
  }

  function appendDataToNewFile(fileName, filePath, sortedFileName) {
    fs.readFile(filePath, "utf-8", function (err, data) {
      if (err) {
        console.log(err);
      } else {
        console.log(`Read ${fileName}`);
        let sortedFilePath = path.join(__dirname, sortedFileName);
        fs.appendFile(sortedFilePath, data, function (err) {
          if (err) {
            console.log(err);
          } else {
            console.log(`${fileName} added to ${sortedFileName}`);
          }
        });
      }
    });
  }

  function sortingContent(sortedFileName) {
    let sortedFilePath = path.join(__dirname, sortedFileName);
    fs.readFile(sortedFilePath, "utf-8", function (err, data) {
      if (err) {
        console.log(err);
      } else {
        console.log(`${sortedFileName} read successfully`);
        let bothSplittedData = data.split("\n");
        let sortedData = bothSplittedData.sort();
        let joinedData = sortedData.join("\n");
        fs.writeFile(sortedFilePath, joinedData, function (err) {
          if (err) {
            console.log(err);
          } else {
            console.log("Great Job Nandy You Sorted Successfully");
            writeFileNameToFileNames(sortedFileName);
          }
        });
      }
    });
  }

  function createAndAppendLowerAndUpperCaseContentToNewFile(
    upperCaseFileName,
    lowerCaseFileName
  ) {
    let upperCaseFilePath = path.join(__dirname, upperCaseFileName);
    let lowerCaseFilePath = path.join(__dirname, lowerCaseFileName);
    let sortedFileName = "sortedContent.txt";
    appendDataToNewFile(upperCaseFileName, upperCaseFilePath, sortedFileName);
    appendDataToNewFile(lowerCaseFileName, lowerCaseFilePath, sortedFileName);
    setTimeout(() => {
      sortingContent(sortedFileName);
    }, 100);
  }

  function readAndConvertContentInToLowerCase(upperCaseFileName) {
    let uppercaseFilePath = path.join(__dirname, upperCaseFileName);

    fs.readFile(uppercaseFilePath, "utf-8", function (err, data) {
      if (err) {
        console.log(err);
      } else {
        console.log("Data read from Uppercase Filename");

        let lowerCaseConvertedData = data.toLowerCase();

        let splittedData = lowerCaseConvertedData.split(".");

        let trimmedData = splittedData.map((eachSentence) =>
          eachSentence.trim()
        );

        let lowerCaseSentence = trimmedData.join(".\n");

        let lowerCaseFileName = "lowerCaseSplittedFile.txt";
        let lowerCaseFilePath = path.join(__dirname, lowerCaseFileName);

        fs.writeFile(lowerCaseFilePath, lowerCaseSentence, function (err) {
          if (err) {
            console.log(err);
          } else {
            console.log("File created with LowerCase Splitted Sentence");
          }
          writeFileNameToFileNames(lowerCaseFileName);
          createAndAppendLowerAndUpperCaseContentToNewFile(
            upperCaseFileName,
            lowerCaseFileName
          );
        });
      }
    });
  }

  function writeUpperCaseDataToNewFile(data) {
    let upperCaseFileName = "upperCaseConvertedFile.txt";
    let upperCaseFilePath = path.join(__dirname, upperCaseFileName);
    let upperCaseConvertedData = data.toUpperCase();

    let splittedUpperCaseData = upperCaseConvertedData.split("\n\n");
    let joinedUpperCaseData = splittedUpperCaseData.join("\n");
    fs.writeFile(upperCaseFilePath, joinedUpperCaseData, function (err) {
      if (err) {
        console.log(err);
      } else {
        console.log("File created with UpperCase Content");
      }
      writeFileNameToFileNames(upperCaseFileName);
      readAndConvertContentInToLowerCase(upperCaseFileName);
    });
  }

  function readFileFunction(file) {
    fs.readFile(file, "utf-8", function (err, data) {
      if (err) {
        console.log(err);
      } else {
        console.log("Data read From lipsum.txt");

        writeUpperCaseDataToNewFile(data);
      }
    });
  }

  readFileFunction(file);
  setTimeout(function () {
    deleteAllFilesByFileName();
  }, 500);
}

module.exports = callbackFunctionProblem2;
